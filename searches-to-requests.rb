#!/usr/bin/env ruby

# Converts a log file export of GitLab searches into the relevant HTTP requests
# for use as example load testing searches

require 'csv'
require 'time'
require 'webmock'
require 'json'

WebMock.enable!

response = {
  hits: {
    total: {
      value: 0
    },
    hits: []
  }
}

WebMock.stub_request(:get, /#{::Gitlab::CurrentSettings.elasticsearch_url.first}/).to_return(body: response.to_json, status: 200, headers: { "Content-Type": "application/json" })

::Gitlab::SafeRequestStore.clear!
::Gitlab::SafeRequestStore.begin!
::Gitlab::SafeRequestStore[:peek_enabled] = true

CSV.foreach('/tmp/recent-searches.csv', headers: true) do |row|

  #time = Time.parse(row["json.time"])
  username = row["json.meta.user"]
  scope = row["json.meta.search.scope"]
  project_id = row["json.meta.search.project_id"]
  group_id = row["json.meta.search.group_id"]

  next unless group_id || project_id

  search = row["json.meta.search.search"].gsub(/[^a-zA-Z]/, ' ')

  user = User.find_by_username(username)
  search_service = ::SearchService.new(user, {scope: scope, search: search, project_id: project_id, group_id: group_id})
  search_service.search_results.objects(scope).count
end; nil

recent_searches_data = ::Gitlab::Instrumentation::ElasticsearchTransport.detail_store.map { |request| request.slice(:path, :params, :body) }; nil

File.open('/tmp/recent-searches.json', 'w') do |file|
  file.write(recent_searches_data.to_json)
end; nil

# httperf output doesn't seem to work
#
# File.open('/tmp/recent-searches_wsesslog.log', 'w') do |requests_file|
#   ::Gitlab::Instrumentation::ElasticsearchTransport.detail_store.each do |request|
#     requests_file.puts("/#{request[:path]} method=POST contents='#{request[:body].to_json}'")
#     requests_file.puts
#   end
# end; nil

