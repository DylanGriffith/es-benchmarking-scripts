#!/usr/bin/env ruby

require 'benchmark'
require 'rest-client'
require 'securerandom'
require 'json'

CLUSTER_URL = ENV.fetch("CLUSTER_URL")
PROJECT_ID = ENV.fetch("PROJECT_ID")
INDEX = ENV.fetch("INDEX")

ISSUE_QUERY = '{ "query": { "bool": { "must": [ { "simple_query_string": { "_name": "issue:match:search_terms", "fields": [ "title^2", "description" ], "query": "<QUERY>", "default_operator": "and" } } ], "filter": [ { "term": { "type": { "_name": "doc:is_a:issue", "value": "issue" } } }, { "has_parent": { "_name": "issue:authorized:project", "parent_type": "project", "query": { "bool": { "should": [ { "bool": { "filter": [ { "terms": { "_name": "issue:authorized:project:membership:id", "id": [ <PROJECT_ID> ] } }, { "terms": { "_name": "issue:authorized:project:issues:enabled_or_private", "issues_access_level": [ 20, 10 ] } } ] } } ] } } } } ] } }, "highlight": { "fields": { "title": {}, "description": {} }, "number_of_fragments": 0, "pre_tags": [ "gitlabelasticsearch→" ], "post_tags": [ "←gitlabelasticsearch" ] } }'
  .sub("<PROJECT_ID>", PROJECT_ID)

CODE_QUERY = '{ "query": { "bool": { "must": [ { "simple_query_string": { "_name": "issue:match:search_terms", "fields": [ "title^2", "description" ], "query": "<QUERY>", "default_operator": "and" } } ], "filter": [ { "term": { "type": { "_name": "doc:is_a:issue", "value": "issue" } } }, { "has_parent": { "_name": "issue:authorized:project", "parent_type": "project", "query": { "bool": { "should": [ { "bool": { "filter": [ { "terms": { "_name": "issue:authorized:project:membership:id", "id": [ <PROJECT_ID> ] } }, { "terms": { "_name": "issue:authorized:project:issues:enabled_or_private", "issues_access_level": [ 20, 10 ] } } ] } } ] } } } } ] } }, "highlight": { "fields": { "title": {}, "description": {} }, "number_of_fragments": 0, "pre_tags": [ "gitlabelasticsearch→" ], "post_tags": [ "←gitlabelasticsearch" ] } }'
  .sub("<PROJECT_ID>", PROJECT_ID)

QUERY_WORDS = ['auto', 'devops', 'hello', 'world', 'something', 'queryable', 'searching', 'somethingelse']

def search(query, term, routing:)
  query = query.sub("<QUERY>", term)
  url = "#{CLUSTER_URL}/#{INDEX}/doc/_search?from=0&size=20" + (routing ? "&routing=project_#{PROJECT_ID}" : '')
  RestClient.post(url, query, {content_type: :json, accept: :json})
end

def random_term
  words = QUERY_WORDS.sample(3).shuffle

  words.join(" ")
end

N = 100
Benchmark.bm(10) do |x|
  x.report("Issues - With routing:") { N.times { search(ISSUE_QUERY, random_term, routing: true) } }
  x.report("Issues - Without routing:") { N.times { search(ISSUE_QUERY, random_term, routing: false) } }
  x.report("Code - With routing:") { N.times { search(CODE_QUERY, random_term, routing: true) } }
  x.report("Code - Without routing:") { N.times { search(CODE_QUERY, random_term, routing: false) } }
end
